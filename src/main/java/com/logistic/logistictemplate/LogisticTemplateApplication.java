package com.logistic.logistictemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogisticTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(LogisticTemplateApplication.class, args);
	}
}
